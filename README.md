# Classes

Это репозиторий для второго пункта марафона по Android-разработке. Решение заданий лежат в отдельных файлах:

1) [Data classes](https://gitlab.com/NikitaEfimov0/classes/-/blob/main/Data_classes.kt)
2) [Smart classes](https://gitlab.com/NikitaEfimov0/classes/-/blob/main/Smart_casts.kt)
3) [Sealed classes](https://gitlab.com/NikitaEfimov0/classes/-/blob/main/Sealed_classes.kt)
4) [Rename on import](https://gitlab.com/NikitaEfimov0/classes/-/blob/main/Rename_on_import.kt)
5) [Extension function](https://gitlab.com/NikitaEfimov0/classes/-/blob/main/Extension_functions.kt)

[Скриншот с подверждением прохождения тестов](https://gitlab.com/NikitaEfimov0/classes/-/blob/main/task2.png)
